registerPatcher({
	info: info,
	gameModes: [xelib.gmTES5, xelib.gmSSE],
	settings: {
		label: 'The Fog Machine',
		templateUrl: `${patcherUrl}/partials/settings.html`,
		defaultSettings: {
			superFog: false,
			subtleFog: false,
		}	
	},
	
	
	requiredFiles: [],
	getFilesToPatch: function(filenames) {
		return filenames;
	},
	execute: (patchFile, helpers, settings, locals) => ({
		process: [{
			load: { //Weathers
				signature: 'WTHR',
				filter: rec => true
			},
			
			//Standard Settings for extra foggy fog
			patch: rec => {
				//Fog doesn't show up without this enabled
				xelib.SetValue(rec, 'NNAM', 'FXCameraAttachFogEffect [RFCT:000B94CD]');
				//I forgot why I enabled this. Oh yeah, fog doesn't work without this on
				xelib.SetFlag(rec, 'DATA\\Flags', 'Weather - Cloudy', true)
				
				if (settings.superFog.enabled) {
					//Standard Settings for extra foggy fog
					xelib.SetValue(rec, 'FNAM\\Day - Near', '0');
					xelib.SetValue(rec, 'FNAM\\Day - Far', '5000');
					xelib.SetValue(rec, 'FNAM\\Night - Near', '0');
					xelib.SetValue(rec, 'FNAM\\Night - Far', '5000');
					//Intensities. DO NOT CHANGE AT ALL, OKAY? YOU'RE GOING TO HAVE A BAD TIME IF YOU DO
					xelib.SetValue(rec, 'FNAM\\Day - Power', '0.4');
					xelib.SetValue(rec, 'FNAM\\Night - Power', '0.4');
					xelib.SetValue(rec, 'FNAM\\Day - Max', '0.65');
					xelib.SetValue(rec, 'FNAM\\Night - Max', '0.65');
					return true
				}
				
				if (settings.subtleFog.enabled) {
					//For weirdos that actually want to see their LoDs
					xelib.SetValue(rec, 'FNAM\\Day - Near', '2000');
					xelib.SetValue(rec, 'FNAM\\Day - Far', '50000');
					xelib.SetValue(rec, 'FNAM\\Night - Near', '2000');
					xelib.SetValue(rec, 'FNAM\\Night - Far', '50000');
					//Intensities. DO NOT CHANGE AT ALL, OKAY? YOU'RE GOING TO HAVE A BAD TIME IF YOU DO
					xelib.SetValue(rec, 'FNAM\\Day - Power', '0.4');
					xelib.SetValue(rec, 'FNAM\\Night - Power', '0.4');
					xelib.SetValue(rec, 'FNAM\\Day - Max', '0.65');
					xelib.SetValue(rec, 'FNAM\\Night - Max', '0.65');
					return true
				}
			}
		}]
	})
});
